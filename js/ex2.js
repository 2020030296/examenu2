function cambiar(origen){
 let MD=document.getElementById('MD');
 switch(origen.value){
    case "MX":
        origen.options[0].hidden=true;
        MD.options[0]= new Option('Dolar Estadounidense', 'DE');
        MD.options[1]= new Option('Dolar Canadiense', 'DC');
        MD.options[2]= new Option('Euro', 'EU');
        break;
    case "DE":
        origen.options[0].hidden=true;
        MD.options[0]= new Option('Peso Mexicano', 'MX');
        MD.options[1]= new Option('Dolar Canadiense', 'DC');
        MD.options[2]= new Option('Euro', 'EU');
        break;
    case "DC":
        origen.options[0].hidden=true;
        MD.options[0]= new Option('Peso Mexicano', 'MX');
        MD.options[1]= new Option('Dolar Estadounidense', 'DE');
        MD.options[2]= new Option('Euro', 'EU');
        break;
    case "EU":
        origen.options[0].hidden=true;
        MD.options[0]= new Option('Peso Mexicano', 'MX');
        MD.options[1]= new Option('Dolar Estadounidense', 'DE');
        MD.options[2]= new Option('Dolar Canadiense', 'DC');
        break;
    default:
        MD.options[0] = new Option('-------',null);
        MD.options[1] = new Option('-------',null);
        MD.options[2] = new Option('-------',null);
        break;

 }   
}

function calcular(){
    let origen = document.getElementById('MO');
    let MD=document.getElementById('MD');
    let cantidad= document.getElementById('can').value;
    let subtotal=0;
    let comision=0;
    let total;

    switch(origen.value){
        case "MX":
            switch(MD.value){
                case "DE":
                    subtotal=(cantidad/19.85).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "DC":
                    subtotal=((cantidad/19.85)*1.35).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "EU":
                    subtotal=((cantidad/19.85)*0.99).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                default:
                   
                    break;   
            }

            break;
        case "DE":
            switch(MD.value){
                case "MX":
                    subtotal=(cantidad*19.85).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "DC":
                    subtotal=(cantidad*1.35).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "EU":
                    subtotal=(cantidad*0.99).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                default:
                   
                    break;   
            }
            break;
        case "DC":
            switch(MD.value){
                case "MX":
                    subtotal=((cantidad*19.85)/1.35).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "DE":
                    subtotal=(cantidad/1.35).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "EU":
                    subtotal=((cantidad*0.99)/19.85).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                default:
                   
                    break;   
            }
            break;
        case "EU":
            switch(MD.value){
                case "MX":
                    subtotal=((cantidad*19.85)/0.99).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "DE":
                    subtotal=(cantidad/0.99).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                case "DC":
                    subtotal=((cantidad*1.35)/0.99).toFixed(2);
                    comision=(subtotal*0.03).toFixed(2);
                    total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                    break;
                default:
                   
                    break;   
            }
            break;
        default:
           
            break;
    
     }   
     document.getElementById('sub').value=subtotal;
    document.getElementById('comi').value=comision;
    document.getElementById('tot').value=total;
}
var acumulacion=0;
function registrar(){
    let acumular=document.getElementById('tot').value;
    let parrafo=document.getElementById('parra');
    let totg= document.getElementById('totalg');
    
    let parrafoa;
    var MO= document.getElementById('MO')[document.getElementById('MO').selectedIndex].text;
    var MD = document.getElementById('MD')[document.getElementById('MD').selectedIndex].text;
    let subtotal=document.getElementById('sub').value;
    let comision=document.getElementById('comi').value;
    let total=document.getElementById('tot').value;
    let cantidad= document.getElementById('can').value;
    parrafoa=cantidad + " | " + MO + " a " + MD + " | " + subtotal + " | "
    + comision + " | " + total + " |<br>";
    parrafo.innerHTML= parrafo.innerHTML + parrafoa;

    acumulacion=  parseFloat(acumular) + acumulacion ;
    totg.innerText=(acumulacion).toFixed(2);
}

function borrar(){
    let parrafo=document.getElementById('parra');
    acumulacion=0;
    parrafo.innerHTML="<br><br>";
    document.getElementById('totalg').innerHTML="0";
}